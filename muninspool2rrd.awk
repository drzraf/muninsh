#!/usr/bin/awk -f

# Copyright (C) 2017, Raphaël . Droz + floss @ gmail DOT com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#
# Attempt to update a munin's master RRD file from a munin-async spoolfetch passed at STDIN
# HowTo:
# $ echo spoolfetch 1510417871 | ssh VM /usr/share/munin/munin-async > data.txt # eventually gzip
# # this will return concatenated (and timestamp'ed values)
#
# cat data.txt | awk -f muninspool2rrd.awk -v prefix=dir/foo/vm | sort -k2,3n | awk -f rrddedup.awk | sh -
# Then combine with original rrd using https://raw.githubusercontent.com/oetiker/rrdtool-rrdjig/master/rrdjig.pl
# rrdjig.pl --src-tmpl=42 --dst-tmpl=42 dir/foo/vm-tcp_states-ESTABLISHED-g.rrd original.rrd
#
# ToDo: update munin state
# $ echo list | ssh VM /usr/share/munin/munin-async

BEGIN {
	path_prefix = "output-dir/foo";
	if (length(prefix) > 0) {
		path_prefix = prefix;
	}
	if (dumpcfg) dumpcfg = 1; else dumpcfg = 0;
	if (create) show_create_cmd = create; else show_create_cmd = 0;

	default_type = "GAUGE";
	default_min = "U";
	default_max = "U";
	default_update_rate = 300;
	default_graph_data_size = "normal";

	graph = "";
	previous_subgraph = subgraph = "";
	cmd_print = 0;
}

{
	if ($0 ~ /^multigraph/) {
		if (!dumpcfg) print("");
		graph = $2;
		# UpdateWorker.pm::_get_rrd_file_name
		gsub(/[:;]/, "/", graph);
		gsub(/\./, "-", graph);
		subgraph = "";
		cmd_print = 1;
		next;
	}

	if ($1 == "graph_type") { default_type = $2; next; }
	if ($1 == "update_rate") { default_update_rate = $2; next; }
	if ($1 == "graph_data_size") {
		sub(/graph_data_size /, "", $0);
		default_graph_data_size = $0;
		next;
	}
	if ($1 ~ /^graph_/) { next; }

	if ($1 ~ /\.(min|max|type|update_rate|graph_data_size|draw|label)$/ ) {
		split($1, a, /\./);
		subgraph = a[1];

		set_default_conf(graph, subgraph);

		if (a[2] == "min") config[graph][subgraph]["min"] = $2;
		else if (a[2] == "max") config[graph][subgraph]["max"] = $2;
		else if (a[2] == "type") config[graph][subgraph]["type"] = $2;
		else if (a[2] == "update_rate") config[graph][subgraph]["update_rate"] = $2;
		else if (a[2] == "graph_data_size") {
			sub(/graph_data_size /, "", $0);
			config[graph][subgraph]["graph_data_size"] = $0;
		}
		next;
	}

	if (dumpcfg) next;

	if ($0 ~ /\.value\s+[0-9]+:.+/) {
		split($1, a, /\./);
		subgraph = a[1];
		split($2, b, /:/);
		timestamp = b[1];

		if (! config[graph][subgraph]["rra"]) {
			 config[graph][subgraph]["file"] = _get_rrd_file_name(graph, subgraph);
			 config[graph][subgraph]["rra"] = cmd_create(graph, subgraph);
		}
		if (! config[graph][subgraph]["first"] || config[graph][subgraph]["first"] > timestamp) {
			config[graph][subgraph]["first"] = timestamp;
		}

		if (previous_subgraph == "" || subgraph != previous_subgraph) {
			print("");
		}

		print_cmd_data_line(subgraph);
	}
}

END {
	print("");
	if (dumpcfg) {
		dump_cfg();
	}

	if (show_create_cmd) {
		for (g in config) {
			for (sg in config[g]) {
				cmd_rra = config[g][sg]["rra"];
				first = config[g][sg]["first"];
				update_rate = config[g][sg]["update_rate"];
				sub(/FIRSTEPOCH/, (first - update_rate), cmd_rra);
				# sub(/FIRSTEPOCH/, 1454544000, v); # 2016-02-03 21:00:00
				print(cmd_rra);
			}
		}
	}
}

function cmd_create(graph, subgraph) {
	start = "FIRSTEPOCH" # placeholder # first_epoch - update_rate;
	return sprintf("rrdcreate %s --start %s --step %s DS:42:%s:%s:%s:%s",
		       _get_rrd_file_name(graph, subgraph),
		       start,
		       config[graph][subgraph]["update_rate"],
		       # DS name
		       config[graph][subgraph]["type"],
		       # heartbeat
		       config[graph][subgraph]["update_rate"] * 2,
		       config[graph][subgraph]["min"],
		       config[graph][subgraph]["max"]) " " get_rras(config[graph][subgraph]["graph_data_size"]);
}

function _get_rrd_file_name(graph, subgraph) {
	# "g" for GAUGE, ...
	type_id = tolower(substr(config[graph][subgraph]["type"], 0, 1));
	return sprintf("%s%s-%s-%s.rrd", path_prefix, graph, subgraph, type_id);
}

function print_cmd_data_line(subgraph) {
	split($1, a, /\./);
	split($2, b, /:/);
	if (cmd_print || previous_subgraph == "" || subgraph != previous_subgraph) {
		printf("rrdupdate %s", _get_rrd_file_name(graph, subgraph));
		cmd_print = 0;
		previous_subgraph = subgraph;
	}
	printf(" %d:%s", b[1], b[2]);
}

function set_default_conf(graph, subgraph) {
	if (!config[graph][subgraph]["type"]) {
		config[graph][subgraph]["type"] = default_type;
	}
	if (! config[graph][subgraph]["min"]) {
		config[graph][subgraph]["min"] = default_min;
	}
	if (! config[graph][subgraph]["max"]) {
		config[graph][subgraph]["max"] = default_max;
	}
	if (! config[graph][subgraph]["update_rate"]) {
		config[graph][subgraph]["update_rate"] = default_update_rate;
	}
	if (! config[graph][subgraph]["graph_data_size"]) {
		config[graph][subgraph]["graph_data_size"] = default_graph_data_size;
	}
}


function get_rras(resolution) {
	if (resolution == "normal") {
		config[graph][subgraph]["update_rate"] = 300;
		# resolution 5 minutes
		return "RRA:AVERAGE:0.5:1:576 " \
			"RRA:MIN:0.5:1:576 "	\
			"RRA:MAX:0.5:1:576 "	\
		# 9 days, resolution 30 minutes
		"RRA:AVERAGE:0.5:6:432 "	\
			"RRA:MIN:0.5:6:432 "	\
			"RRA:MAX:0.5:6:432 "	\
		# 45 days, resolution 2 hours
		"RRA:AVERAGE:0.5:24:540 "     \
			"RRA:MIN:0.5:24:540 " \
			"RRA:MAX:0.5:24:540 " \
		# 450 days, resolution 1 day
		"RRA:AVERAGE:0.5:288:450 "	\
			"RRA:MIN:0.5:288:450 "	\
			"RRA:MAX:0.5:288:450" ;
	}

	else if (resolution == "huge") {
		config[graph][subgraph]["update_rate"] = 300;
		# resolution 5 minutes, for 400 days
		return "RRA:AVERAGE:0.5:1:115200 " \
			"RRA:MIN:0.5:1:115200 "	   \
			"RRA:MAX:0.5:1:115200";
	}

	else if (resolution ~ /^custom (.+)/) {
		sub(/custom /, "", resolution);
		# Parsing resolution to achieve computer format as defined on the RFC :
		# FULL_NB, MULTIPLIER_1 MULTIPLIER_1_NB, ... MULTIPLIER_NMULTIPLIER_N_NB
		res = parse_custom_resolution(resolution, config[graph][subgraph]["update_rate"]);
		return res;
	}
}


function parse_custom_resolution(res, update_rate) {
	split(res, elems, /,\s*/);

	# First element is always the full resolution
	full_res = elems[1];
	if (full_res ~ /^[0-9]+$/) {
		# Only numeric, computer format
		elems[1] = "1 " full_res;
	} else {
		# Human readable. Adding $update_rate in front of
		elems[1] = update_rate " for " full_res;
	}

	i = 1;
        for (elem in elems) {
                if (elems[elem] ~ /([0-9]+) ([0-9]+)/) {
			split(elems[elem], d, / /);
                        # nothing to do, already in computer format
                        computer_format[i][1] = d[1];
                        computer_format[i][2] = d[2];
                }
		else if (elems[elem] ~ /\w+ for \w+/) {
			split(elems[elem], d, / for /);
                        nb_sec = to_sec(d[1]);
                        for_sec = to_sec(d[2]);
			multiplier = int (nb_sec / update_rate);
                        multiplier_nb = int (for_sec / nb_sec);

			# print( "[DEBUG] " elems[elem] " -> nb_sec:" nb_sec ", for_sec:" for_sec " -> multiplier:" multiplier ", multiplier_nb:" multiplier_nb );
                        computer_format[i][1] = multiplier;
			computer_format[i][2] = multiplier_nb;
                }
		i += 1;
	}

	# in original perl code: resolutions_computer = computer_format;

	ret = "";
	for(j in computer_format) {
		multiplier = computer_format[j][1];
		multiplier_nb = computer_format[j][2];
		# http://munin-monitoring.org/wiki/format-graph_data_size
		mul_add = int(multiplier_nb / 10);
		if (! mul_add) mul_add = 1;
		multiplier_nb += mul_add;
		ret = sprintf("%1$s RRA:AVERAGE:0.5:%2$s:%3$s " \
			      "RRA:MIN:0.5:%2$s:%3$s " \
			      "RRA:MAX:0.5:%2$s:%3$s", ret, multiplier, multiplier_nb);
	}

	# print("from res " res " to " ret); exit;
        return ret;
}

function to_sec(str) {
	if (str ~ /[0-9]+s/) return strtonum(str) * 1;
	else if (str ~ /[0-9]+m/) return strtonum(str) * 60;
	else if (str ~ /[0-9]+h/) return strtonum(str) * 60 * 60;
	else if (str ~ /[0-9]+d/) return strtonum(str) * 60 * 60 * 24;
	else if (str ~ /[0-9]+w/) return strtonum(str) * 60 * 60 * 24 * 7;
	else if (str ~ /[0-9]+t/) return strtonum(str) * 60 * 60 * 24 * 31;
	else if (str ~ /[0-9]+y/) return strtonum(str) * 60 * 60 * 24 * 365;
	else return int(str);
}

function dump_cfg() {
	for(i in config) {
		print(i);
		for(j in config[i]) {
			if (!isarray(config[i][j])) {
				printf("\t%s - %s\n", j, config[i][j]);
			}
			else {
				print("\t\t", j);
				for(k in config[i][j]) {
					print("\t\t\t " k " = " config[i][j][k]);
				}
			}
		}
	}
}
