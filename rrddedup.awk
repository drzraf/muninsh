#!/usr/bin/awk -f

# Copyright (C) 2017, Raphaël . Droz + floss @ gmail DOT com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

BEGIN {
    previous_filename = "";
}

{
    filename = $2

    if ($0 ~ /rrd *create/) {
	    if (createifnotexist) {
		    if ($1 == "rrdcreate") file = $2
		    else file = $3;
		    print("[ -f " file " ] || " $0);
	    }
	    else {
		    print($0);
	    }
	    previous_filename = "";
	    next;
    }

    if (! filename) { next; }
    
    if (! previous_filename) {
	printf($0);
	previous_filename = $2;
	next;
    }

    if (filename == previous_filename) {
	printf(" " $3);
    } else {
	print("");
	printf($0);
    }

    previous_filename = filename
}

END {
    print("");
}
