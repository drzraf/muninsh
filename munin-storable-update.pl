#!/usr/bin/perl -w

# Copyright (C) 2017, Raphaël . Droz + floss @ gmail DOT com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version

use warnings;
use Storable;
use Storable qw(store_fd fd_retrieve);
use Data::Dumper; 

if ($ARGV[0] !~ m/^[0-9]+$/) {
    print "\nUsage: plstorable.pl timestamp\n";
    exit;
}

$hashref = fd_retrieve(*STDIN);
$hashref->{spoolfetch} = int($ARGV[0]);
print STDERR Dumper($hashref);
store_fd($hashref, *STDOUT);
