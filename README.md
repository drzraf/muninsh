# Munin'sh: Munin master/node using simple shellscript

## Problem 1: munin-async (node) eats a lot of memory  

If a munin-sync stops working a couple of days, /var/lib/munin-async fills up with more metric data
 than what the available memory allows.  
 `/usr/share/munin/munin-async` load the [whole file concatenation in memory](https://github.com/munin-monitoring/munin/blob/master/lib/Munin/Node/SpoolReader.pm#L93). Hardcoded [magic value](https://github.com/munin-monitoring/munin/blob/master/lib/Munin/Node/SpoolReader.pm#L113) exists to reduce the risk of OOM at the price of missing data but this is less than ideal.  
[munin-quick-async](./munin-quick-async) attempt to solve this problem. It's low-memory equivalent to munin-async's spoolfetch command.

## Problem 2: munin-update (master) eats a lot of memory  

Munin's master [UpdateWorker.pm](https://github.com/munin-monitoring/munin/blob/master/lib/Munin/Master/UpdateWorker.pm) does a lot of things rather than simply updating RRD files and is deeply intricated to the whole Munin-update process. Updating RRD-tool manually from an external spoolfetch is not possible.  
This is the purpose of [muninspool2rrd.awk](./muninspool2rrd.awk) + [rrddedup.awk](./rrddedup.awk)

## Problem 3: munin-update relies upon Perl Storable

munin-update system is not foolproof and breaks easily. munin-update depends upon a timestamp store inside `/var/lib/munin/state-<group>-<node>.storable`.
If this file is deleted, spoolfetch requests data since 1970 which will probably trigger an annoying OOM.
[munin-storable-update.pl](./munin-storable-update.pl) is a simple wrapper around Storable to manually reset the timestamp to a correct value after a manual update of RRD files.


# Example:

## Part 1: saving node's spoolfetch
```sh
node$ du -sh /var/lib/munin-async
776M  /var/lib/munin-async
node$ echo spoolfetch $(date -d "now - 15 days" +%s) | /usr/share/munin/munin-async
Out of memory
node$ munin-quick-async $(date -d "now - 15 days" +%s) | gzip -9 > spoolfetch-saved.txt.gz
node$ ls -lh spoolfetch-saved.txt.gz
8.5M spoolfetch-saved.txt.gz
```

## Part 2: loading the spoolfetch on the master
```sh
master$ ts=$(date -d "now - 15 days" +%s)
master$ scp node:spoolfetch-saved.txt.gz
master# cp -r /var/lib/munin{,.save}
master$ munin-shupdate | sh -  ## warning: first review/modify munin-shupdate to your configuration
```

* [muninspool2rrd.awk](./muninspool2rrd.awk) basically process the main configuration combined to the spool fetched manually and output rrdtool commands
* sort + [rrdedup.awk](./rrdedup.awk) represent a useful optimization pass by creating one unique call to `rrdupdate` for a given file from multiple `rrdupdate` input lines

## Part 3: update Munin storable timestamp

Since RRD are updated, last-updated-timestamp need to be updated for Munin master to fetch at the correct date next time it's run. But this is inside a Perl *.storable.

An alias that can help:
`alias pdump='perl -e "use Data::Dumper;use Storable;print(Dumper(Storable::fd_retrieve(*STDIN)));"'`

```
master# munin-storable-update.pl $ts < /var/lib/munin.save/state-mygroup-node.storable > /var/lib/munin/state-mygroup-node.storable
master# sudo -u munin /usr/share/munin/munin-update --nofork --host node
master# sudo -u munin /usr/share/munin/munin-html
```
Graphs regenerated and fixed? No missing data anymore? Then `/usr/bin/munin-cron` can be uncommented again inside `/etc/cron.d/munin`
